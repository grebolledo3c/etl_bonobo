import bonobo

def extract():
    import requests
    import re
    import camelot
    from bs4 import BeautifulSoup
    import pandas as pd

    #scraping y obtención del enlace del último pdf generado con los datos de la disponibilidad de cama
    download_url = "https://www.icovidchile.cl/informes"

    headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Max-Age': '3600',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'
    }

    req = requests.get(download_url, headers)
    soup = BeautifulSoup(req.content, "html.parser")
    soup = BeautifulSoup(soup.prettify(), "html.parser")
    a = soup.find_all("a", href=re.compile("https://uploads.strikinglycdn.com/files/"))
    url_pdf = a[0].get('href')

    #Lectura de las tablas en el PDF
    tables = camelot.read_pdf(url_pdf, pages="1-end")
    df_camas = tables[0].df

    # extraemos los datos de la positividad del último informe Minsal
    positividad=pd.read_json("https://raw.githubusercontent.com/MinCiencia/Datos-COVID19/master/output/producto12/bulk/producto7.json")

    # extraemos los datos de los fallecidos del último informe Minsal
    fallecidos=pd.read_csv("https://raw.githubusercontent.com/MinCiencia/Datos-COVID19/master/output/producto11/bulk/producto4.csv")

    # extraemos los datos de los datos diarios del último informe Minsal
    datos_diarios=pd.read_csv("https://raw.githubusercontent.com/MinCiencia/Datos-COVID19/master/output/producto3/TotalesPorRegion_std.csv")

    yield [df_camas, positividad, fallecidos, datos_diarios]

def clean(df, *args):
    df_camas = df[0] 
    df[0] = df_camas.drop(df_camas.index[0: 3])
    yield df

def transform(df, *args):
    region_to_filter = "Biobío"
    #solo dejamos las columnas 0 y 6 y el resto las eliminamos
    df_camas = df[0]
    df_camas = df_camas[[0, 6]]
    df_camas.columns = ['region', '% ocupacion de camas']
    df[0] = df_camas
    
    df_positividad = df[1]
    positividad_biobio = df_positividad[df_positividad.Region==region_to_filter]
    positividad_biobio = df_positividad[["Fecha","PCR Realizados","Tasa"]]
    df[1] = positividad_biobio

    df_fallecidos = df[2]
    fallecidos_biobio=df_fallecidos[df_fallecidos.Region==region_to_filter]
    fallecidos_biobio=fallecidos_biobio[["Fecha","Fallecidos"]]
    df[2] = fallecidos_biobio

    df_datos_diarios = df[3]
    datos_diarios_biobio= df_datos_diarios[df_datos_diarios.Region=="Biobío"]
    casos_nuevos=datos_diarios_biobio[datos_diarios_biobio.Categoria=="Casos nuevos totales"]
    casos_nuevos=casos_nuevos[["Fecha","Total"]]
    df[3] = casos_nuevos

    yield df

def load(df, *args):
    import json
    """Placeholder, change, rename, remove... """
    data = {'ultimo_informe_camas': df[0].to_dict(orient="records"), 'positividad': df[1].to_dict(orient="records"), 'fallecidos': df[2].to_dict(orient="records"), 'casos_nuevos': df[3].to_dict(orient="records")}
    json = json.dumps(data);

    with open('all.json', 'w') as outfile:
        outfile.write(json)

    with open('camas.json', 'w') as outfile:
        outfile.write(df[0].to_json(orient="records", force_ascii=False))

    with open('positividad.json', 'w') as outfile:
        outfile.write(df[1].to_json(orient="records", force_ascii=False))

    with open('fallecidos.json', 'w') as outfile:
        outfile.write(df[2].to_json(orient="records", force_ascii=False))

    with open('casos_nuevos.json', 'w') as outfile:
        outfile.write(df[3].to_json(orient="records", force_ascii=False))

def get_graph(**options):
    """
    This function builds the graph that needs to be executed.

    :return: bonobo.Graph

    """
    graph = bonobo.Graph()
    graph.add_chain(extract, clean, transform, load)

    return graph

def get_services(**options):
    """
    This function builds the services dictionary, which is a simple dict of names-to-implementation used by bonobo
    for runtime injection.

    It will be used on top of the defaults provided by bonobo (fs, http, ...). You can override those defaults, or just
    let the framework define them. You can also define your own services and naming is up to you.

    :return: dict
    """
    return {}


# The __main__ block actually execute the graph.
if __name__ == '__main__':
    parser = bonobo.get_argument_parser()
    with bonobo.parse_args(parser) as options:
        bonobo.run(
            get_graph(**options),
            services=get_services(**options)
        )